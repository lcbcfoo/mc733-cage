PERF_PARAMS=-e cache-misses,branches,instructions,branch-misses,cpu-cycles,page-faults,mem-stores -o perf_out.out


run : FORCE
	perf stat $(PERF_PARAMS) python3 faceswap.py convert

FORCE : ;

benchmark/get_results : benchmark/get_results.cpp
	g++ $< -o $@

bench : benchmark/get_results run
	benchmark/get_results perf_out.out
	cat benchmark.out

clean: FORCE
	rm -f output/* *.out benchmark/get_results out_dir/*.out

install:
	virtualenv -p python3 faceswap_env/
	source faceswap_env/bin/activate; \
	pip3 install -r requirements.txt; \


