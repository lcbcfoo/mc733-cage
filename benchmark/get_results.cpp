#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

#define NFRAMES 10.0

using namespace std;

typedef struct Results{
    long long cache_misses;
    long long instructions;
    long long cpu_cycles;
    long double IPC;
    long long page_faults;
    long long mem_stores;
    long long branches;
    long long branches_missed;
    long double branch_percentage;
    long double time;
    long double score;
} Results;

string remove_commas(string str){
    str.erase(std::remove(str.begin(), str.end(), ','), str.end());
    return str;
}

void write_result(ofstream& output, Results res){
    output  << "Cache misses: " << res.cache_misses << '\n';
    output  << "Branches: " << res.branches << '\n';
    output  << "Total instructions: " << res.instructions << '\n';
    output  << "Branches Missed: " << res.branches_missed << '\n';
    output  << "CPU Cycles: " << res.cpu_cycles << '\n';
    output  << "Page Faults: " << res.page_faults << '\n';
    output  << "Memory stores: " << res.mem_stores << '\n';
    output  << "Time: " << res.time << '\n';
    output  << "Inst per cycle: " << res.IPC << '\n';
    output  << "Branch percentage: " << res.branch_percentage << '\n';
    output  << "Final score: " << res.score << " FPS\n";
}

Results read(ifstream& input){
    string line;
    Results res;
    for(int i = 0; i < 5; i++)
        getline(input, line);

    getline(input, line);
    line = remove_commas(line);
    res.cache_misses = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.branches = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.instructions = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.branches_missed = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.cpu_cycles = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.page_faults = stoll(line, NULL, 10);

    getline(input, line);
    line = remove_commas(line);
    res.mem_stores = stoll(line, NULL, 10);

    // empty line
    getline(input, line);
    getline(input, line);
    line = remove_commas(line);
    res.time = stod(line, NULL);

    res.IPC = (long double) res.instructions / res.cpu_cycles;
    res.branch_percentage = (long double) res.branches_missed / res.branches;

    res.score =  (long double)  NFRAMES / (long double) res.time;

    return res;
}

int main(int argc, char** argv){
    if(argc != 2){
        cout << "Usage: ./<name> <name of perf output>\n";
        return 0;
    }

    string file_name = argv[1];
    string out_name = "benchmark.out";
    ifstream input;
    ofstream output;

    input.open(file_name, ios::in);

    if(!input.is_open()){
        cout << "Could not open file.\n";
        return 0;
    }
    output.open(out_name, ios::out);

    if(!output.is_open()){
        cout << "Could not open out file.\n";
        return 0;
    }

    Results res = read(input);
    write_result(output, res);
    input.close();
    output.close();

    return 0;
}
