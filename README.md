# Cagemark - Grupo 2
## O que faz? Para que serve?
O projeto é de deep learning, onde, após uma etapa de treinamento, o software será capaz de substituir a face de pessoas em vídeos pelo rosto do Nicolas Cage. O benchmark será calcular a eficiência do computador ao executar esse software.
## Por que é bom para medir desempenho?
Como é um software de deep learning, existe um grande número de dados e operações matemáticas, caracterizando uma carga elevada de computação.
## O que baixar
Basta clonar esse repositorio. ELE EH GRANDE PARA AS MAQUINAS DO IC, USE O DIRETORIO /tmp. 
```
cd /tmp
mkdir bench_cage
cd bench_cage
git clone https://lcbcfoo@bitbucket.org/lcbcfoo/mc733-cage.git
```
## Como compilar/instalar
O benchmark usa python3 e alguns modulos que precisam ser instalados atraves do comando `pip3`. Para rodar isso no IC usamos um virtualenv ( https://virtualenv.pypa.io/en/stable/ ). O processo de criacao e preparacao do ambiente foi automatizado, basta executar:
```
cd mc733-cage
make install
```
Vai aparecer uma mensagem dizendo que o virtualenv foi configurado com sucesso.
## Como executar
A partir desse ponto, basta rodar:
```
source faceswap_env/bin/activate
make bench
```
O resultado do benchmark sera colocado em um arquivo `benchmark.out`.
Por fim voce precisa sair do virtualenv com
```
deactivate
```
## Como medir o desempenho
O desempenho eh medido com parametros do programa `perf` que medem os seguintes aspectos do programa rodado: 

* Cache misses
* Total instructions
* CPU Cycles
* Instructions per cycle
* Branches taken
* Branches Missed
* Branch missed percentage
* Page Faults
* Memory stores instructions
* Time of execution
* Final score

O programa executado utiliza aprendizado de maquina para trocar faces de pessoas pela face do Nicolas Cage. Como caracteristica desse tipo de programa, existe uso excessivo do processador e alta quantidade de instrucoes de store na memoria o que permite testar eficientemente a quantidade de page faults. O tempo levado em consideracao eh o tempo que o tempo real que o tempo precisou para terminar. O motivo para utilizacao desse tempo eh que, dado ao uso excessivo de memoria, o tempo de usuario e sistema poderiam ser influenciados pelo alto numero de blocks por page faults.
A entrada eh fixa (a menos que as imagens do diretorio input sejam modificadas), dessa forma a medida de tempo nao sera influencia em diferentes execucoes, ocorrendo apenas oscilacoes, por isso uma medida basta.

## Como apresentar o desempenho
O desempenho sera calculado como o numero de frames convertidos por segundo. Apesar de aparentemente simplistico julgamos que essa metrica eh a que melhor reflete o desempenho geral do sistema ja que todos os fatores envolvidos, como cache misses, branches, instrucoes por segundo, etc contribuiem individualmente para um melhor resultado. Isto eh possivel para esse programa pois o tempo de execucao eh grande e a tarefa eh heterogenea o suficiente, sao utilizadas extensivamente operacoes de ponto flutuante e inteiras, acesso a memoria e ao disco tanto em termos de escrita e leitura.
## Medições base (uma máquina)
O resultado a seguir foi obtido com as seguintes configuracoes:
* Intel(R) Core(TM) i5-4200M CPU @ 2.50GHz
* 4 cores
* L1d cache:           32K
* L1i cache:           32K
* L2 cache:            256K
* L3 cache:            3072K
* 8 GB RAM
* 500GB SSHD

```
Cache misses: 115043094
Branches: 9858465666
Total instructions: 162643078952
Branches Missed: 104996770
CPU Cycles: 80081396901
Page Faults: 1592049
Memory stores: 14147606030
Time: 23.2527
Inst per cycle: 2.03097
Branch percentage: 0.0106504
Final Score: 0.430057 FPS
```